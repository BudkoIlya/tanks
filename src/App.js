import React, {useEffect, useState} from 'react';
import './App.css';

function App() {
  let [top, setTop] = useState(0)
  let [left, setLeft] = useState(0)
  let [rotate, setRotate] = useState('0deg')
  useEffect(()=> {
    window.addEventListener('keydown',({keyCode}) => {
      console.log("Event", keyCode)
      if (keyCode === 40) { // down
        setTop(top+=5)
        setRotate('90deg')
      } else if (keyCode === 39) { // right
        setLeft(left+=5)
        setRotate('0deg')
      } else if (keyCode === 37) { // left
        setLeft(left-=5)
        setRotate('-180deg')
      } else {
        setTop(top-=5)
        setRotate('-90deg')
      }
    })
  },[])
  return (
    <div className="App" >
      <div className='field'/>
      <div className='tank' style={{top,left,transform: `rotate(${rotate})`}}>
        <div className='gun'/>
      </div>
    </div>
  );
}

export default App;
